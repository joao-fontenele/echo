package server

import (
	"context"
	"encoding/json"
	"net"
	"net/http"
	"os"

	"github.com/go-chi/chi/v5"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/rs/zerolog/pkgerrors"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/reflection"

	echopb "gitlab.com/joao-fontenele/echo/proto"
)

type Server struct {
	echopb.UnimplementedEchoServiceServer
}

func (s *Server) Echo(ctx context.Context, req *echopb.EchoRequest) (*echopb.EchoResponse, error) {
	log.Info().Msg("grpc")
	return &echopb.EchoResponse{Message: req.Message}, nil
}

func (s *Server) HTTPHandler() http.Handler {
	r := chi.NewRouter()

	r.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			e := log.Info().Str("method", r.Method).Str("path", r.RequestURI).Str("remoteAddr", r.RemoteAddr)
			for k, v := range r.Header {
				e = e.Strs("header."+k, v)
			}
			e.Msg("http")

			next.ServeHTTP(w, r)
		})
	})

	r.Get("/internal/healthz", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		_, _ = w.Write([]byte("{\"healthy\": \"true\"}"))
	})

	r.Post("/echo", func(w http.ResponseWriter, r *http.Request) {
		var req echopb.EchoRequest
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			http.Error(w, "Bad request", http.StatusBadRequest)
			return
		}
		conn, err := grpc.Dial(":50051", grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			http.Error(w, "Service unavailable", http.StatusServiceUnavailable)
			return
		}
		defer conn.Close()
		client := echopb.NewEchoServiceClient(conn)
		res, err := client.Echo(context.Background(), &req)
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		_ = json.NewEncoder(w).Encode(res)
	})

	return r
}

func (s *Server) Start() {
	s.Configure()
	log.Info().Str("version", viper.GetString("CommitHash")).Msg("")

	grpcServer := grpc.NewServer()
	echopb.RegisterEchoServiceServer(grpcServer, s)
	reflection.Register(grpcServer)
	lis, err := net.Listen("tcp", ":50051")
	if err != nil {
		log.Fatal().Err(err).Msg("failed to listen")
	}
	log.Info().Str("port", lis.Addr().String()).Msg("gRPC server listening on")

	go func() {
		if err := grpcServer.Serve(lis); err != nil {
			log.Fatal().Err(err).Msg("failed to serve")
		}
	}()

	log.Info().Msg("HTTP server listening on :8080")
	if err := http.ListenAndServe(":8080", s.HTTPHandler()); err != nil {
		log.Fatal().Err(err).Msg("failed to serve")
	}
}

func (s *Server) Configure() {
	if viper.GetString("CommitHash") == "local" {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	}

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if viper.IsSet("logLevel") {
		switch viper.GetString("logLevel") {
		case "debug":
			zerolog.SetGlobalLevel(zerolog.DebugLevel)
		case "info":
			zerolog.SetGlobalLevel(zerolog.InfoLevel)
		case "warn":
			zerolog.SetGlobalLevel(zerolog.WarnLevel)
		case "error":
			zerolog.SetGlobalLevel(zerolog.ErrorLevel)
		}
	}

	zerolog.ErrorStackMarshaler = pkgerrors.MarshalStack

	log.Logger = log.With().Timestamp().Caller().Logger()
}
