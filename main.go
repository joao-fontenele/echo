package main

import (
	"gitlab.com/joao-fontenele/echo/cmd"
)

func main() {
	cmd.Execute()
}
