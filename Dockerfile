FROM golang:1.21 as builder

ENV APP /app

RUN mkdir -p ${APP}
WORKDIR ${APP}

COPY go.mod go.sum ${APP}/
RUN go mod download

COPY . ${APP}/
RUN CGO_ENABLED=0 go build -ldflags="-s -w -X gitlab.com/joao-fontenele/echo/cmd.CommitHash=$(git rev-parse HEAD)" -o bin/echo-cli main.go

FROM gcr.io/distroless/base

ENV APP /app
WORKDIR ${APP}

COPY --from=builder ${APP}/bin/echo-cli ${APP}/bin/echo-cli

CMD ["/app/bin/echo-cli", "server"]
